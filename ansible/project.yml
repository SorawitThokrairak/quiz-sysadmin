---
- hosts: ansible-mgn
  become: yes
  become_user: root
  tasks:
  - name: Install NTP
    yum: 
      name: ntp 
      state: latest

  - name: Edit file ntp.conf
    lineinfile:
      path: /etc/ntp.conf
      regexp: '{{ item.match }}'
      line: '{{ item.paste }}'
    loop:
      - { match: '^#restrict 192.168.1.0', paste: 'restrict 192.168.100.0 mask 255.255.255.0 nomodify notrap' }
      - { match: '^server 0', paste: 'server 2.th.pool.ntp.org iburst' }
      - { match: '^server 1', paste: 'server 0.asia.pool.ntp.org iburst' }
      - { match: '^server 2', paste: 'server 2.asia.pool.ntp.org iburst' }
      - { match: '^server 3', paste: '' }

  - name: Start firewalld
    service:
        name: firewalld
        state: started	

  - name: Open firewalld nfs service
    firewalld:
       service: ntp
       permanent: yes
       state: enabled

  - name: Restart firewalld
    service:
        name: firewalld
        state: restarted

  - name: set timezone to Asia/Bangkok
    timezone: 
      name: Asia/Bangkok	

- hosts: myserver
  become: yes
  become_user: root
  tasks:
  - name: Install NTP
    yum: 
      name: ntp 
      state: latest

  - name: Edit file ntp.conf
    lineinfile:
      path: /etc/ntp.conf
      regexp: '{{ item.match }}'
      line: '{{ item.paste }}'
    loop:
      - { match: '^#restrict 192.168.1.0', paste: 'restrict 192.168.100.0 mask 255.255.255.0 nomodify notrap' }
      - { match: '^server 0', paste: '192.168.100.11' }
      - { match: '^server 1', paste: '' }
      - { match: '^server 2', paste: '' }
      - { match: '^server 3', paste: '' }

  - name: Start firewalld
    service:
        name: firewalld
        state: started	

  - name: Open firewalld nfs service
    firewalld:
       service: ntp
       permanent: yes
       state: enabled

  - name: Restart firewalld
    service:
        name: firewalld
        state: restarted

  - name: set timezone to Asia/Bangkok
    timezone: 
      name: Asia/Bangkok	

- hosts: all
  become: yes
  become_user: root
  tasks:
  - name: Install bind-utils
    yum:
     name: bind
     state: latest
    when: ansible_connection == 'local'

  - name: Edit file named.conf listen-on port
    lineinfile:
      path: /etc/named.conf
      regexp: 'listen-on port 53 { 127.0.0.1; };'
      line: 'listen-on port 53 { 127.0.0.1; 192.168.100.11; };'
      backup: yes
    when: ansible_connection == 'local'

  - name: Edit named.conf nameserver allow query 
    lineinfile:
      path: /etc/named.conf
      regexp: 'allow-query     { localhost; };'
      line: 'allow-query     { any; };'
      backup: yes
    when: ansible_connection == 'local'

  - name: add Include named local to named.conf
    lineinfile:
      path: /etc/named.conf
      line: 'include "/etc/named/named.conf.local";'
      backup: yes
    when: ansible_connection == 'local'

  - name: Copy named.conf.local file 
    template:
     src: template/named.conf.local.j2
     dest: /etc/named/named.conf.local
     owner: root
     group: root
     mode: 0755
    when: ansible_connection == 'local'

  - name: Create Zones
    file:
     path: /etc/named/zones
     state: directory
     mode: 0755
    when: ansible_connection == 'local'

  - name: Copy Zone file
    template:
     src: '{{ item.source }}'
     dest: '{{ item.destination }}'
     owner: root
     group: root
     mode: "0755"
    loop:
         - { source: 'template/db.192.168.100.j2', destination: '/etc/named/zones/db.192.168.100' }
         - { source: 'template/db.eatandtour-bangsaen.com.j2', destination: '/etc/named/zones/db.eatandtour-bangsaen.com' }
    when: ansible_connection == 'local'

  - name: Start service named
    service:
        name: named
        state: started
    when: ansible_connection == 'local'

  - name: Start firewalld
    service:
        name: firewalld
        state: started	
    when: ansible_connection == 'local'

  - name: Open firewalld tcp udp
    firewalld:
       port: '{{ item }}'
       permanent: yes
       state: enabled
    loop:
        - "53/udp" 
        - "53/tcp" 
    when: ansible_connection == 'local'
   

  - name: Restart firewalld
    service:
        name: firewalld
        state: restarted	
    when: ansible_connection == 'local'

  - name: Copy resolv file 
    template:
      src: template/resolv.conf.j2
      dest: /etc/resolv.conf
      owner: root
      group: root
      mode: "0644"

  - name: Install Bind-Utils all Server
    yum:
      name: bind-utils
      state: latest

- hosts: web
  become: yes
  become_user: root
  tasks:
  - name: Install remi repo.
    yum:
      name: http://rpms.famillecollet.com/enterprise/remi-release-7.rpm
      state: present

  - name: Import remi GPG key.
    rpm_key:
      key: http://rpms.remirepo.net/RPM-GPG-KEY-remi
      state: present

  - name: Install httpd
    yum:
      name: httpd
      state: latest

  - name: Install php 7.3 and php module
    yum:
      name:
        - php 
        - php-cli 
        - php-fpm 
        - php-mysqlnd 
        - php-zip 
        - php-devel 
        - php-gd 
        - php-mcrypt 
        - php-mbstring 
        - php-curl 
        - php-xml 
        - php-pear 
        - php-bcmath 
        - php-json
      state: latest
      enablerepo: remi-php73

  - name: Edit Timezone php
    lineinfile:
      path: /etc/php.ini
      regexp: ';date.timezone'
      line: 'date.timezone = Asia/Bangkok'
    
  - name: Enable service httpd
    service:
      name: httpd
      enabled: yes

  - name: Start service httpd
    service:
      name: httpd
      state: started

  - name: Enable firewall
    service:
      name: firewalld
      enabled: yes

  - name: Start firewall
    service:
      name: firewalld
      state: started

  - name: Open firewalld service http and https
    firewalld:
       service: '{{ item }}'
       permanent: yes
       state: enabled
    loop:
        - "http" 
        - "https" 
 
  - name: Restart firewalld
    service:
      name: firewalld
      state: restarted	

  - name: Copy file httpd
    template:
      src: 'template/httpd.conf.j2'
      dest: '/etc/httpd/conf/httpd.conf'
      owner: root
      group: root

- hosts: db
  become: yes
  become_user: root
  vars:
    mysql_root_password: 123456
  tasks:
  - name: install mariadb
    yum:
      name: mariadb-server
      state: latest

  - name: install nfs-util in server04
    yum:
      name: nfs-utils
      state: latest

  - name: enable service mariadb
    service:
      name: mariadb
      enabled: yes

  - name: start mariadb
    service:
      name: mariadb
      state: started
  
  - name: enabled firewalld
    service:
      name: firewalld
      enabled: yes

  - name: start firewalld
    service:
      name: firewalld
      state: started
  
  - name: open firewalld tcp
    firewalld:
      port: '3306/tcp'
      permanent: yes
      state: enabled

  - name: set client in etc/exports
    lineinfile:
      path: /etc/exports
      line: '/var/nfs *(rw,sync,no_root_squash,no_all_squash)'

  - name: Create directory /var/nfs
    file:
      path: /var/nfs
      state: directory
      owner: nfsnobody
      group: nfsnobody
      mode: 0755

  - name: Enable service
    service: 
       name: '{{ item }}'
       enabled: yes
    loop:
        - "rpcbind" 
        - "nfs-server"
        - "nfs-lock"

  - name: start service nfs
    service: 
       name: '{{ item }}'
       state: started
    loop:
        - "rpcbind" 
        - "nfs-server"
        - "nfs-lock"
      
  - name: Open firewalld nfs service
    firewalld:
       service: '{{ item }}'
       permanent: yes
       state: enabled
    loop:
        - "nfs" 
        - "mountd"
        - "rpc-bind"
  
  - name: Restart firewalld
    service:
      name: firewalld
      state: restarted

- hosts: web
  become: yes
  become_user: root
  tasks:
  - name: install nfs-util
    yum:
      name: nfs-utils
      state: latest

  - name: Create dir nfs
    file:
      path: '/var/nfs/www'
      state: directory
      mode: 0755

  - name: Mount directory
    command: mount 192.168.100.70:/var/nfs /var/nfs/www


  - name: Create dir log for eatandtour-bangsaen
    file:
      path: '/var/nfs/www/eatandtour-bangsaen.com/log'
      state: directory
      mode: 0755

  - name: Create dir html eatandtour-bangsaen
    file:
      path: '/var/nfs/www/eatandtour-bangsaen.com/html'
      state: directory
      mode: 0755

  - name: Copy file conf to server02
    template:
      src: template/eatandtour-bangsaen.com.conf.j2
      dest: /etc/httpd/conf.d/eatandtour-bangsaen.com.conf
      owner: root
      group: root
      mode: "0755"
    when: ansible_ssh_host == '192.168.100.81'
  
  - name: Copy file conf to server03
    template:
      src: template/eatandtour-bangsaen.com.conf.j2
      dest: /etc/httpd/conf.d/eatandtour-bangsaen.com.conf
      owner: root
      group: root
      mode: "0755"
    when: ansible_ssh_host == '192.168.100.82'

  - name: Copy index.php to web.server
    template:
      src: template/index.php.j2
      dest: /var/nfs/www/eatandtour-bangsaen.com/html/index.php
      owner: nfsnobody
      group: nfsnobody
      mode: 0755

  - name: Restart service httpd
    service:
      name: httpd
      state: restarted

- hosts: lb
  become: yes
  become_user: root
  tasks:
  - name: install nginx
    yum:
      name: nginx
      state: latest
  
  - name: Enable nginx
    service: 
       name: nginx
       enabled: yes

  - name: start service nginx
    service: 
       name: nginx
       state: started

  - name: Open firewalld 80/tcp
    firewalld:
       port: '80/tcp'
       permanent: yes
       state: enabled

  - name: Open firewalld http
    firewalld:
       service: 'http'
       permanent: yes
       state: enabled
  
  - name: create file eatandtour-bangsaen.conf
    file:
      path: /etc/nginx/conf.d/eatandtour-bangsaen.conf
      state: touch
      owner: root
      group: root
      mode: 0755

    
  - name: Copy file eatandtour-bangsaen.conf
    template:
      src: template/eatandtour-bangsaen.conf.j2
      dest: /etc/nginx/conf.d/eatandtour-bangsaen.conf
      owner: root
      group: root
      mode: 0755

  - name: Restart service nginx
    service:
      name: nginx
      state: restarted

  - name: Restart firewalld
    service:
      name: firewalld
      state: restarted	

- hosts: all
  become: yes
  become_user: root
  tasks:
  - name: Enable service rsyslog
    service:
      name: rsyslog
      enabled: yes

  - name: Start service rsyslog
    service:
      name: rsyslog
      state: started

  - name: open rsyslog by uncomment TCP UDP
    template:
      src: template/rsyslog.conf.j2
      dest: /etc/rsyslog.conf
      owner: root
      group: root
      mode: 0755
    when: ansible_connection == 'local'

  - name: edit config remote-host rsyslog
    lineinfile:
      path: /etc/rsyslog.conf
      regexp: '#*.* @@remote-host:514'
      line: '*.* @192.168.100.11:514'

  - name: restart rsyslog
    service:
      name: rsyslog
      state: restarted

  - name: Open firewalld udp tcp
    firewalld:
       port: '{{ item }}'
       permanent: yes
       state: enabled
    loop:
        - "514/udp" 
        - "514/tcp" 

  - name: Restart firewalld
    service:
      name: firewalld
      state: restarted	


    